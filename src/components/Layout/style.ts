import styled from "styled-components";
import { HexColor } from "../../styles/constants";

export const Container = styled.div`
  display: grid;
  margin: 0 0.5rem;
  grid-template-areas:
    "header header header"
    ". main .";
  grid-auto-columns: auto 1fr;
  grid-gap: 0.5rem;
`;

export const Header = styled.header`
  grid-area: header;
  border-bottom: 1px solid ${HexColor.BLACK};
`;

export const Title = styled.h1`
  font-size: 1.65rem;
  margin: 0.5rem 0;
  font-weight: 500;
`;

export const Content = styled.main`
  grid-area: main;
  max-width: 1396px;
  place-self: center;
  width: 100%;
`;
