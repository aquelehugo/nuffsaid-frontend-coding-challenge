import { render, screen } from "@testing-library/react";
import Layout from ".";

describe("Component: Layout", () => {
  it("renders a heading", () => {
    render(<Layout />);

    const heading = screen.getByRole("heading");
    expect(heading).toBeInTheDocument();
  });

  it("renders children", () => {
    render(
      <Layout>
        <button type="button">Test button</button>
      </Layout>
    );

    const testButton = screen.getByRole("button", { name: "Test button" });
    expect(testButton).toBeInTheDocument();
  });
});
