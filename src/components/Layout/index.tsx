import React from "react";
import { Header, Container, Title, Content } from "./style";

const Layout: React.FC = ({ children }) => (
  <Container>
    <Header>
      <Title>nuffsaid.com Coding Challenge</Title>
    </Header>
    <Content>{children}</Content>
  </Container>
);

export default Layout;
