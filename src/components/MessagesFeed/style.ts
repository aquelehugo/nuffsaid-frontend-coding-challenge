import styled from 'styled-components';

export const Container = styled.section`
  display: grid;
  gap: 0.75rem;
  place-content: flex-start;
  grid-template-columns: 1fr;
`;

export const Header = styled.header`
`;

export const Title = styled.h2`
`;

export const Counter = styled.span`
`;
