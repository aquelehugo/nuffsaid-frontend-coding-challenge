import React, { useMemo } from "react";
import { useMessagingContext } from "../../contexts/messaging";
import { Priority } from "../../services/Api";
import MessageCard from "../MessageCard";
import { Container, Title, Counter, Header } from "./style";

type MessagesFeedProps = {
  priority: Priority;
  title: string;
};

const MessagesFeed = ({ priority, title }: MessagesFeedProps) => {
  const { messages, removeMessage } = useMessagingContext();

  const filteredMessages = useMemo(
    () => messages.filter((message) => message.data.priority === priority),
    [messages, priority]
  );

  return (
    <Container>
      <Header>
        <Title>{title}</Title>
        <Counter>Count {filteredMessages.length}</Counter>
      </Header>
      {filteredMessages.map((message) => (
        <MessageCard
          key={message.id}
          message={message}
          onClear={removeMessage}
        />
      ))}
    </Container>
  );
};

export default MessagesFeed;
