import { render, screen } from "@testing-library/react";
import MessagesFeed from ".";
import MessagingContextMock from "../../contexts/messaging/mock";
import { Priority } from "../../services/Api";
import { AppMessage } from "../../types";

describe("Component: MessagesFeed", () => {
  it("renders the title", () => {
    render(<MessagesFeed title="Test title" priority={Priority.Info} />);

    const title = screen.getByRole("heading", { name: "Test title" });
    expect(title).toBeInTheDocument();
  });

  it("renders the messages from context filtered by priority", () => {
    const errorMessages: AppMessage[] = [
      {
        id: 1,
        data: {
          message: "Error message 1",
          priority: Priority.Error,
        },
      },
      {
        id: 2,
        data: {
          message: "Error message 2",
          priority: Priority.Error,
        },
      },
    ];

    const messages: AppMessage[] = [
      ...errorMessages,
      {
        id: 3,
        data: {
          message: "Info message 1",
          priority: Priority.Info,
        },
      },
      {
        id: 4,
        data: {
          message: "Warn message 1",
          priority: Priority.Warn,
        },
      },
    ];

    render(
      <MessagingContextMock
        value={{
          messages,
        }}
      >
        <MessagesFeed title="Test title" priority={Priority.Error} />
      </MessagingContextMock>
    );

    const errorMessageElements = errorMessages.map(errorMessage => screen.getByText(errorMessage.data.message));
    errorMessageElements.forEach(errorMessageElement => expect(errorMessageElement).toBeInTheDocument());
  });

  it("renders the count of messages from context filtered by priority", () => {
    const errorMessages: AppMessage[] = [
      {
        id: 1,
        data: {
          message: "Error message 1",
          priority: Priority.Error,
        },
      },
      {
        id: 2,
        data: {
          message: "Error message 2",
          priority: Priority.Error,
        },
      },
    ];

    const messages: AppMessage[] = [
      ...errorMessages,
      {
        id: 3,
        data: {
          message: "Info message 1",
          priority: Priority.Info,
        },
      },
      {
        id: 4,
        data: {
          message: "Warn message 1",
          priority: Priority.Warn,
        },
      },
    ];

    render(
      <MessagingContextMock
        value={{
          messages,
        }}
      >
        <MessagesFeed title="Test title" priority={Priority.Error} />
      </MessagingContextMock>
    );

    const errorMessagesCount = errorMessages.length;
    const counter = screen.getByText(`Count ${errorMessagesCount}`);
    expect(counter).toBeInTheDocument();
  });
});
