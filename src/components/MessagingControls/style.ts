import Button from "@material-ui/core/Button";
import styled from "styled-components";

export const StyledButton = styled(Button)`
  && {
    font-weight: bold;
    font-family: sans-serif;
  }
`;
