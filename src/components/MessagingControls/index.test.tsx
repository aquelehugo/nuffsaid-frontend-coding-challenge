import { fireEvent, render, screen } from "@testing-library/react";
import MessagingControls from ".";
import MessagingContextMock from "../../contexts/messaging/mock";
import { MessagingStatus } from "../../contexts/messaging/types";

describe("Component: MessagingControls", () => {
  it("renders a clear button", () => {
    render(<MessagingControls />);

    const clearButton = screen.getByRole("button", { name: "clear" });
    expect(clearButton).toBeInTheDocument();
  });

  it("renders a stop button when messaging status is accepting", () => {
    render(
      <MessagingContextMock
        value={{
          status: MessagingStatus.ACCEPTING,
        }}
      >
        <MessagingControls />
      </MessagingContextMock>
    );

    const stopButton = screen.getByRole("button", { name: "stop" });
    expect(stopButton).toBeInTheDocument();
  });

  it("renders a resume button when messaging status is ignoring", () => {
    render(
      <MessagingContextMock
        value={{
          status: MessagingStatus.IGNORING,
        }}
      >
        <MessagingControls />
      </MessagingContextMock>
    );

    const resumeButton = screen.getByRole("button", { name: "resume" });
    expect(resumeButton).toBeInTheDocument();
  });

  it("calls clearMessages from context when clear button is clicked", () => {
    const clearMessages = jest.fn();

    render(
      <MessagingContextMock
        value={{
          clearMessages,
        }}
      >
        <MessagingControls />
      </MessagingContextMock>
    );

    const clearButton = screen.getByRole("button", { name: "clear" });
    fireEvent.click(clearButton);

    expect(clearMessages).toHaveBeenCalled();
  });

  it("calls acceptMessages from context when resume is clicked", () => {
    const acceptMessages = jest.fn();

    render(
      <MessagingContextMock
        value={{
          status: MessagingStatus.IGNORING,
          acceptMessages,
        }}
      >
        <MessagingControls />
      </MessagingContextMock>
    );

    const resumeButton = screen.getByRole("button", { name: "resume" });
    fireEvent.click(resumeButton);

    expect(acceptMessages).toHaveBeenCalled();
  });

  it("calls ignoreMessages from context when stop is clicked", () => {
    const ignoreMessages = jest.fn();

    render(
      <MessagingContextMock
        value={{
          status: MessagingStatus.ACCEPTING,
          ignoreMessages,
        }}
      >
        <MessagingControls />
      </MessagingContextMock>
    );

    const stopButton = screen.getByRole("button", { name: "stop" });
    fireEvent.click(stopButton);

    expect(ignoreMessages).toHaveBeenCalled();
  });
});
