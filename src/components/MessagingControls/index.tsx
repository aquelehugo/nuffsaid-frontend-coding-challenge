import React from "react";
import { useMessagingContext } from "../../contexts/messaging";
import { MessagingStatus } from "../../contexts/messaging/types";
import { StyledButton } from "./style";

const MessagingControls = () => {
  const { acceptMessages, ignoreMessages, clearMessages, status } =
    useMessagingContext();

  const stopResumeButtonProps =
    status === MessagingStatus.ACCEPTING
      ? {
          onClick: ignoreMessages,
          children: "stop",
        }
      : {
          onClick: acceptMessages,
          children: "resume",
        };

  return (
    <>
      <StyledButton
        variant="contained"
        color="primary"
        {...stopResumeButtonProps}
      />
      <StyledButton variant="contained" color="primary" onClick={clearMessages}>
        clear
      </StyledButton>
    </>
  );
};

export default MessagingControls;
