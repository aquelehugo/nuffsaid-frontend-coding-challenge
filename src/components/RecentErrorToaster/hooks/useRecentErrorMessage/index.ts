import { useEffect, useRef, useState } from "react";
import { useMessagingContext } from "../../../../contexts/messaging";
import { Priority } from "../../../../services/Api";
import { AppMessage } from "../../../../types";

const useRecentErrorMessage = () => {
  const { messages } = useMessagingContext();
  const lastMessagesLength = useRef<number>(0);
  const [recentErrorMessage, setRecentErrorMessage] =
    useState<AppMessage | null>(null);

  useEffect(() => {
    const messageIsIncoming = messages.length > lastMessagesLength.current;
    lastMessagesLength.current = messages.length;

    const [latestMessage] = messages;
    const incomingError =
      messageIsIncoming &&
      latestMessage &&
      latestMessage.data.priority === Priority.Error
        ? latestMessage
        : null;

    if (incomingError) {
      setRecentErrorMessage(incomingError);
      return;
    }
  }, [messages]);

  return recentErrorMessage;
};

export default useRecentErrorMessage;
