import Alert from "@material-ui/lab/Alert";
import Snackbar, { SnackbarProps } from "@material-ui/core/Snackbar";
import React, { useEffect, useState } from "react";

import useRecentErrorMessage from "./hooks/useRecentErrorMessage";
import { Button } from "@material-ui/core";

const RecentErrorToaster = () => {
  const recentErrorMessage = useRecentErrorMessage();
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(!!recentErrorMessage);
  }, [recentErrorMessage]);

  const close = () => setOpen(false);

  const onClose: SnackbarProps["onClose"] = (event, reason) => {
    if (reason !== "clickaway") {
      close();
    }
  };

  if (!recentErrorMessage) {
    return null;
  }

  return (
    <Snackbar
      key={recentErrorMessage.id}
      open={open}
      autoHideDuration={2000}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      onClose={onClose}
    >
      <Alert
        severity="error"
        action={
          <Button size="small" onClick={close}>
            dismiss
          </Button>
        }
      >
        {recentErrorMessage.data.message}
      </Alert>
    </Snackbar>
  );
};

export default RecentErrorToaster;
