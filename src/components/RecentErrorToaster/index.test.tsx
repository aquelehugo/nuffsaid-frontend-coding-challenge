import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { useEffect } from "react";
import { useState } from "react";
import RecentErrorToaster from ".";
import MessagingContextMock from "../../contexts/messaging/mock";
import { Priority } from "../../services/Api";
import { AppMessage } from "../../types";

describe("Component: RecentErrorToaster", () => {
  const errorMessages: AppMessage[] = ["Error 1", "Error 2", "Error 3"].map(
    (message) => ({
      id: Math.random(),
      data: {
        message,
        priority: Priority.Error,
      },
    })
  );
  const infoMessage = {
    id: Math.random(),
    data: {
      message: "Info message 1",
      priority: Priority.Info,
    },
  };
  const warnMessage = {
    id: Math.random(),
    data: {
      message: "Warn message 1",
      priority: Priority.Warn,
    },
  };

  afterEach(() => {
    jest.useRealTimers();
  });

  it("renders the most recent message from context when it is an error", () => {
    const messages: AppMessage[] = [...errorMessages, infoMessage, warnMessage];

    render(
      <MessagingContextMock value={{ messages }}>
        <RecentErrorToaster />
      </MessagingContextMock>
    );

    const snackbarMessage = screen.getByText(errorMessages[0].data.message);
    expect(snackbarMessage).toBeInTheDocument();
  });

  it("does not render when the most recent message from context is not an error", () => {
    const messages: AppMessage[] = [infoMessage, ...errorMessages, warnMessage];

    const { container } = render(
      <MessagingContextMock value={{ messages }}>
        <RecentErrorToaster />
      </MessagingContextMock>
    );

    expect(container.firstChild).toBeNull();
  });

  it("hides after 2s", async () => {
    jest.useFakeTimers();
    const messages: AppMessage[] = [...errorMessages, infoMessage, warnMessage];

    render(
      <MessagingContextMock value={{ messages }}>
        <RecentErrorToaster />
      </MessagingContextMock>
    );

    const snackbarMessage = screen.getByText(errorMessages[0].data.message);
    expect(snackbarMessage).toBeInTheDocument();

    act(() => {
      jest.advanceTimersByTime(2000);
    });
    await waitFor(() => expect(snackbarMessage).not.toBeInTheDocument());
  });

  it("hides when dismiss button is clicked", async () => {
    const messages: AppMessage[] = [...errorMessages, infoMessage, warnMessage];

    render(
      <MessagingContextMock value={{ messages }}>
        <RecentErrorToaster />
      </MessagingContextMock>
    );

    const snackbarMessage = screen.getByText(errorMessages[0].data.message);
    expect(snackbarMessage).toBeInTheDocument();

    const dismissButton = screen.getByRole("button", { name: "dismiss" });
    fireEvent.click(dismissButton);

    await waitFor(() => expect(snackbarMessage).not.toBeInTheDocument());
  });

  it("replaces the rendered message when a new error message arrives", async () => {
    const initialError = errorMessages[0];
    const newError = errorMessages[1];

    const TestComponent = () => {
      const [messages, setMessages] = useState([initialError]);

      useEffect(() => {
        setTimeout(() => {
          setMessages([newError, initialError]);
        }, 500);
      }, []);

      return (
        <MessagingContextMock value={{ messages }}>
          <RecentErrorToaster />
        </MessagingContextMock>
      );
    };

    render(<TestComponent />);

    const initialSnackbarMessage = screen.getByText(initialError.data.message);
    expect(initialSnackbarMessage).toBeInTheDocument();

    const newSnackbarMessage = await screen.findByText(newError.data.message);
    expect(newSnackbarMessage).toBeInTheDocument();
    expect(initialSnackbarMessage).not.toBeInTheDocument();
  });
});
