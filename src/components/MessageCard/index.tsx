import React from "react";
import { AppMessage } from "../../types";
import {
  StyledButton,
  StyledCard,
  StyledCardActions,
  StyledCardContent,
} from "./style";

type MessageCardProps = {
  message: AppMessage;
  onClear: (messageId: AppMessage["id"]) => void;
};

const MessageCard = ({ message, onClear }: MessageCardProps) => {
  return (
    <StyledCard elevation={1} raised priority={message.data.priority}>
      <StyledCardContent>{message.data.message}</StyledCardContent>
      <StyledCardActions>
        <StyledButton onClick={() => onClear(message.id)}>clear</StyledButton>
      </StyledCardActions>
    </StyledCard>
  );
};

export default React.memo(MessageCard);
