import { fireEvent, render, screen } from "@testing-library/react";
import MessageCard from ".";
import { Priority } from "../../services/Api";
import { AppMessage } from "../../types";

describe("Component: MessageCard", () => {
  const testMessage: AppMessage = {
    id: Math.random(),
    data: {
      priority: Priority.Info,
      message: "Test message",
    },
  };

  it("renders the message", () => {
    render(<MessageCard message={testMessage} onClear={() => {}} />);

    const message = screen.getByText(testMessage.data.message);
    expect(message).toBeInTheDocument();
  });

  it("renders a clear button", () => {
    render(<MessageCard message={testMessage} onClear={() => {}} />);

    const clearButton = screen.getByRole("button", { name: "clear" });
    expect(clearButton).toBeInTheDocument();
  });

  it("calls onClear with message id when the clear button is clicked", () => {
    const onClear = jest.fn();
    render(<MessageCard message={testMessage} onClear={onClear} />);

    const clearButton = screen.getByRole("button", { name: "clear" });
    fireEvent.click(clearButton);

    expect(onClear).toHaveBeenCalledWith(testMessage.id);
  });
});
