import { Button, Card, CardActions, CardContent } from "@material-ui/core";
import styled from "styled-components";
import { Priority } from "../../services/Api";
import theme from "../../styles/theme";

type StyledCardProps = {
  priority: Priority;
};

const backgroundByPriority: Record<Priority, string> = {
  [Priority.Error]: theme.palette.error.main,
  [Priority.Warn]: theme.palette.warning.main,
  [Priority.Info]: theme.palette.info.main,
};

export const StyledCard = styled(Card)<StyledCardProps>`
  && {
    background: ${({ priority }) => backgroundByPriority[priority]};
  }
`;

export const StyledCardContent = styled(CardContent)`
  && {
    padding-bottom: 0;
  }
`;

export const StyledCardActions = styled(CardActions)`
  && {
    justify-content: flex-end;
  }
`;

export const StyledButton = styled(Button)`
  && {
    text-transform: capitalize;
  }
`;
