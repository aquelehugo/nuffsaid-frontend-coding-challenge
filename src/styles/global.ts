import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    border: 0;
    padding: 0;
    background: none;
    box-sizing: border-box;
  }
`;

export default GlobalStyles;
