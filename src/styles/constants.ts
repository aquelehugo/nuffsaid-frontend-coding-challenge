export enum HexColor {
  BLACK = '#000000',
  WHITE = '#FFFFFF',
  RED = '#F56236',
  YELLOW = '#FCE788',
  GREEN = '#88FCA3'
}
