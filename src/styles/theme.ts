import createTheme from "@material-ui/core/styles/createTheme";
import { HexColor } from "./constants";

const theme = createTheme({
  palette: {
    primary: {
      main: HexColor.GREEN,
    },
    info: {
      main: HexColor.GREEN,
    },
    warning: {
      main: HexColor.YELLOW,
    },
    error: {
      main: HexColor.RED,
    },
  },
});

export default theme;
