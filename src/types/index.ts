import { Message } from "../services/Api";

export type AppMessage = {
  id: number;
  data: Message;
}
