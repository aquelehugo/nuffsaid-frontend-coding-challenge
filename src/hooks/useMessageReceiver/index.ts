import { useEffect } from "react";
import { useMessagingContext } from "../../contexts/messaging";
import generateMessage from "../../services/Api";

const useMessageReceiver = () => {
  const { addMessage } = useMessagingContext();

  useEffect(() => {
    const cleanUp = generateMessage(addMessage);
    return cleanUp;
  }, [addMessage]);
};

export default useMessageReceiver;
