import { render } from "@testing-library/react";
import MessagesPane from ".";

describe("View: MessagesPane", () => {
  it("renders", () => {
    const { container } = render(<MessagesPane />);
    expect(container.firstChild).toBeInTheDocument();
  });
});
