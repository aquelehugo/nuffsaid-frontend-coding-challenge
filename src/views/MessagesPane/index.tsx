import React from "react";
import MessagesFeed from "../../components/MessagesFeed";
import MessagingControls from "../../components/MessagingControls";
import RecentErrorToaster from "../../components/RecentErrorToaster";
import useMessageReceiver from "../../hooks/useMessageReceiver";
import { Priority } from "../../services/Api";
import { Container, ControlsArea, FeedsArea } from "./style";

const MessagesPane = () => {
  useMessageReceiver();

  return (
    <Container>
      <ControlsArea>
        <MessagingControls />
      </ControlsArea>
      <FeedsArea>
        <MessagesFeed title="Error Type 1" priority={Priority.Error} />
        <MessagesFeed title="Warning Type 2" priority={Priority.Warn} />
        <MessagesFeed title="Info Type 3" priority={Priority.Info} />
      </FeedsArea>
      <RecentErrorToaster />
    </Container>
  );
};

export default MessagesPane;
