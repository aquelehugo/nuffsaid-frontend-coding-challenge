import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid-column-gap: 1rem;
  grid-row-gap: 5rem;
  grid-template-rows: auto 1fr;
`;

export const FeedsArea = styled.section`
  display: grid;
  grid-auto-flow: column;
  grid-auto-columns: 1fr;
  grid-gap: 1rem;
`;

export const ControlsArea = styled.section`
  display: grid;
  grid-auto-flow: column;
  justify-content: center;
  grid-gap: 0.25rem;
`;
