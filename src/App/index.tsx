import { ThemeProvider } from "@material-ui/core";
import React from "react";
import Layout from "../components/Layout";
import { MessagingContextProvider } from "../contexts/messaging";
import GlobalStyles from "../styles/global";
import theme from "../styles/theme";
import MessagesPane from "../views/MessagesPane";

const App: React.FC<{}> = () => (
  <>
    <ThemeProvider theme={theme}>
      <Layout>
        <MessagingContextProvider>
          <MessagesPane />
        </MessagingContextProvider>
      </Layout>
    </ThemeProvider>
    <GlobalStyles />
  </>
);

export default App;
