import React from "react";
import { DEFAULT_MESSAGING_CONTEXT, MessagingContext } from ".";
import { MessagingContextType } from "./types";

type MessagingContextMockProps = {
  value: Partial<MessagingContextType>;
};

const MessagingContextMock: React.FC<MessagingContextMockProps> = ({
  children,
  value,
}) => (
  <MessagingContext.Provider
    value={{
      ...DEFAULT_MESSAGING_CONTEXT,
      ...value,
    }}
  >
    {children}
  </MessagingContext.Provider>
);

export default MessagingContextMock;
