import { AppMessage } from "../../types";

export enum MessagingStatus {
  ACCEPTING = "accepting",
  IGNORING = "ignoring",
}

export type MessagingContextType = {
  messages: AppMessage[];
  status: MessagingStatus;

  addMessage: (data: AppMessage["data"]) => void;
  removeMessage: (messageId: AppMessage["id"]) => void;
  clearMessages: () => void;
  acceptMessages: () => void;
  ignoreMessages: () => void;
};
