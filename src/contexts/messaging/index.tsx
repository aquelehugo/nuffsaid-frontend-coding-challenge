import React, { createContext, useContext } from "react";
import useMessagingProvider from "./hooks/useMessagingProvider";
import { MessagingContextType, MessagingStatus } from "./types";

export const DEFAULT_MESSAGING_CONTEXT: MessagingContextType = {
  messages: [],
  status: MessagingStatus.ACCEPTING,
  addMessage: () => {},
  removeMessage: () => {},
  clearMessages: () => {},
  acceptMessages: () => {},
  ignoreMessages: () => {},
};

export const MessagingContext = createContext<MessagingContextType>(
  DEFAULT_MESSAGING_CONTEXT
);

export const MessagingContextProvider: React.FC = ({ children }) => {
  const messagingProvider = useMessagingProvider();

  return (
    <MessagingContext.Provider value={messagingProvider}>
      {children}
    </MessagingContext.Provider>
  );
};

export const useMessagingContext = () => useContext(MessagingContext);
