import { useMemo, useReducer } from "react";
import { AppMessage } from "../../../../types";
import { MessagingContextType, MessagingStatus } from "../../types";

enum MessagingServiceActionType {
  ADD = "add",
  REMOVE = "remove",
  CLEAR_ALL = "clear-all",
  ACCEPT = "accept",
  IGNORE = "ignore",
}

type MessagingServiceState = {
  messages: AppMessage[];
  lastMessageId: AppMessage["id"];
  status: MessagingStatus;
};

type MessagingServiceAction =
  | {
      type: MessagingServiceActionType.ADD;
      data: AppMessage["data"];
    }
  | {
      type: MessagingServiceActionType.REMOVE;
      messageId: number;
    }
  | {
      type:
        | MessagingServiceActionType.CLEAR_ALL
        | MessagingServiceActionType.ACCEPT
        | MessagingServiceActionType.IGNORE;
    };

type MessagingReducer = React.Reducer<
  MessagingServiceState,
  MessagingServiceAction
>;

const messagingReducersByAction: Record<
  MessagingServiceActionType,
  MessagingReducer
> = {
  [MessagingServiceActionType.ADD]: (state, action) =>
    action.type === MessagingServiceActionType.ADD &&
    state.status === MessagingStatus.ACCEPTING
      ? {
          ...state,
          lastMessageId: state.lastMessageId + 1,
          messages: [
            {
              id: state.lastMessageId + 1,
              data: action.data,
            },
            ...state.messages,
          ],
        }
      : state,

  [MessagingServiceActionType.REMOVE]: (state, action) =>
    action.type === MessagingServiceActionType.REMOVE
      ? {
          ...state,
          messages: state.messages.filter(({ id }) => id !== action.messageId),
        }
      : state,

  [MessagingServiceActionType.CLEAR_ALL]: (state) => ({
    ...state,
    messages: [],
  }),

  [MessagingServiceActionType.ACCEPT]: (state) => ({
    ...state,
    status: MessagingStatus.ACCEPTING,
  }),

  [MessagingServiceActionType.IGNORE]: (state) => ({
    ...state,
    status: MessagingStatus.IGNORING,
  }),
};

const useMessagingProvider = (): MessagingContextType => {
  const [{ messages, status }, dispatch] = useReducer<
    React.Reducer<MessagingServiceState, MessagingServiceAction>
  >((state, action) => messagingReducersByAction[action.type](state, action), {
    messages: [],
    lastMessageId: 0,
    status: MessagingStatus.ACCEPTING,
  });

  const actions = useMemo(
    () => ({
      addMessage: (data: AppMessage["data"]) =>
        dispatch({
          type: MessagingServiceActionType.ADD,
          data,
        }),
      removeMessage: (messageId: AppMessage["id"]) =>
        dispatch({
          type: MessagingServiceActionType.REMOVE,
          messageId,
        }),
      clearMessages: () =>
        dispatch({
          type: MessagingServiceActionType.CLEAR_ALL,
        }),
      acceptMessages: () =>
        dispatch({
          type: MessagingServiceActionType.ACCEPT,
        }),
      ignoreMessages: () =>
        dispatch({
          type: MessagingServiceActionType.IGNORE,
        }),
    }),
    [dispatch]
  );

  return {
    messages,
    status,
    ...actions,
  };
};

export default useMessagingProvider;
