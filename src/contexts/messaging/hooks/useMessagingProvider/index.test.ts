import { renderHook, act } from "@testing-library/react-hooks";
import useMessagingProvider from ".";
import { Message, Priority } from "../../../../services/Api";
import { MessagingStatus } from "../../types";

describe("Hook: useMessagingProvider", () => {
  it("starts with empty messages array", () => {
    const { result } = renderHook(() => useMessagingProvider());
    expect(result.current.messages).toHaveLength(0);
  });

  it("starts with accepting status", () => {
    const { result } = renderHook(() => useMessagingProvider());
    expect(result.current.status).toBe(MessagingStatus.ACCEPTING);
  });

  it("adds messages to the start of the array when addMessage is called", () => {
    const { result } = renderHook(() => useMessagingProvider());

    const firstMessageToAdd: Message = {
      message: "first message",
      priority: Priority.Info,
    };
    const secondMessageToAdd: Message = {
      message: "second message",
      priority: Priority.Info,
    };

    act(() => result.current.addMessage(firstMessageToAdd));
    expect(result.current.messages[0].data).toBe(firstMessageToAdd);

    act(() => result.current.addMessage(secondMessageToAdd));
    expect(result.current.messages[0].data).toBe(secondMessageToAdd);
    expect(result.current.messages).toHaveLength(2);
  });

  it("removes messages by id when removeMessage is called", () => {
    const { result } = renderHook(() => useMessagingProvider());

    const firstMessageToAdd: Message = {
      message: "first message",
      priority: Priority.Info,
    };
    const secondMessageToAdd: Message = {
      message: "second message",
      priority: Priority.Info,
    };

    act(() => result.current.addMessage(firstMessageToAdd));
    act(() => result.current.addMessage(secondMessageToAdd));
    expect(result.current.messages).toHaveLength(2);

    const [keptMessage, removedMessage] = result.current.messages;
    act(() => result.current.removeMessage(removedMessage.id));
    expect(result.current.messages).toHaveLength(1);
    expect(result.current.messages[0]).toBe(keptMessage);
  });

  it("clears all messages when clearMessages is called", () => {
    const { result } = renderHook(() => useMessagingProvider());

    const firstMessageToAdd: Message = {
      message: "first message",
      priority: Priority.Info,
    };
    const secondMessageToAdd: Message = {
      message: "second message",
      priority: Priority.Info,
    };

    act(() => result.current.addMessage(firstMessageToAdd));
    act(() => result.current.addMessage(secondMessageToAdd));
    expect(result.current.messages).toHaveLength(2);

    act(() => result.current.clearMessages());
    expect(result.current.messages).toHaveLength(0);
  });

  it("sets status as ignoring when ignoreMessages is called", () => {
    const { result } = renderHook(() => useMessagingProvider());

    act(() => result.current.ignoreMessages());
    expect(result.current.status).toBe(MessagingStatus.IGNORING);
  });

  it("sets status as accepting again when acceptMessages is called", () => {
    const { result } = renderHook(() => useMessagingProvider());

    act(() => result.current.ignoreMessages());
    act(() => result.current.acceptMessages());
    expect(result.current.status).toBe(MessagingStatus.ACCEPTING);
  });

  it("skips adding messages when status is ignoring", () => {
    const { result } = renderHook(() => useMessagingProvider());

    act(() => result.current.ignoreMessages());
    act(() =>
      result.current.addMessage({
        message: "test message",
        priority: Priority.Warn,
      })
    );

    expect(result.current.messages).toHaveLength(0);
  });

  it("allows removing messages when status is ignoring", () => {
    const { result } = renderHook(() => useMessagingProvider());

    act(() =>
      result.current.addMessage({
        message: "test message",
        priority: Priority.Warn,
      })
    );
    expect(result.current.messages).toHaveLength(1);

    act(() => result.current.ignoreMessages());
    act(() => result.current.removeMessage(result.current.messages[0].id));
    expect(result.current.messages).toHaveLength(0);
  });

  it("allows clearing all messages when clearMessages status is ignoring", () => {
    const { result } = renderHook(() => useMessagingProvider());

    const firstMessageToAdd: Message = {
      message: "first message",
      priority: Priority.Info,
    };
    const secondMessageToAdd: Message = {
      message: "second message",
      priority: Priority.Info,
    };

    act(() => result.current.addMessage(firstMessageToAdd));
    act(() => result.current.addMessage(secondMessageToAdd));
    expect(result.current.messages).toHaveLength(2);

    act(() => result.current.ignoreMessages());
    act(() => result.current.clearMessages());
    expect(result.current.messages).toHaveLength(0);
  });
});
